import java.sql.SQLOutput;

public class DriverPassager {
    private String name;
    private int passager;

    public DriverPassager(String driver, int passager) {

        this.name = driver;
        this.passager = passager;
    }

    public DriverPassager() {
    }

    public String getDriver() {
        return name;
    }

    public void setDriver(String name) {
        this.name = name;
    }

    public int getPassager() {
        return passager;
    }

    public void setPassager(int passager) {
        this.passager = passager;
    }




    @Override
    public String toString() {
        return "DriverPassenger{" +
                "name='" + name + '\'' +
                ", passenger=" + passager +
                '}';
    }



}
