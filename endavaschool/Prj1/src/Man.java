import java.util.Objects;

public class Man extends Car {
    private String name;
    private int age;
    private Integer age1;

    public Man() {
    }

    public Man(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Man(String name, Integer age1) {
        this.name = name;
        this.age1 = age1;
    }

    public Integer getAge1() {
        return age1;
    }

    public void setAge1(Integer age1) {
        this.age1 = age1;
    }

    public Man(String color, String model, String typeOfBox, int capacity, String driver,
               int passenger, String name, int age) {
        super(color, model, typeOfBox, capacity, driver, passenger);
        this.name = name;
        this.age = age;
        this.age1 = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Man man = (Man) o;
        return age == man.age &&
                name.equals(man.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void walk() {
        System.out.println("Hello! I am walking.");
    }

    public void eat() {
        System.out.println("Good food!");
    }

    @Override
    public String toString() {
        StringBuffer str = new StringBuffer(" I am your Driver for today. ");
        str.append("My name is " + name +
                ", I am " + age +
                " years old.");
        return str.toString();
    }

    @Override
    public void accelerate(String string) {

        System.out.println("Are we ride or not?");
        if (string == "Yes") {
            System.out.println(string + ", We are!");
        } else {
            System.out.println(string + ", we aren't. ");
        }
    }
    public void speed() {
        System.out.println("We have an automatic. ");
    }
    @Override
    public void speed(int speed) {
        System.out.println("Let's see the speed!It is " + speed);
    }
}
