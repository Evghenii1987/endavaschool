import java.util.Objects;

public class Car implements Manipulation{
    private String color;
    private String model;
    private String typeOfBox;
    private int capacity;

    public Car() {
    }
     DriverPassager driverPassager= new DriverPassager();
//    public Car(int capacity, String driver, int passenger) {
//
//
//
//
//    }
    public Car(String color, String model, String typeOfBox, int capacity, String driver, int passenger) {
        this.color = color;
        this.model = model;
        this.typeOfBox = typeOfBox;

        try{
            if (passenger <= capacity){
                this.capacity=capacity;
                driverPassager.setPassager(passenger);
                driverPassager.setDriver(driver);
                System.out.println("We can go! We are "+driverPassager.getPassager()+
                        " in the car. Our driver is "+ driverPassager.getDriver());
            }else{
                throw new Exception();

            }
        }catch (Exception e){
            this.capacity=capacity;
            driverPassager.setPassager(passenger);
            driverPassager.setDriver(driver);
            System.out.println(passenger+" Is to much passenger");
        }
    }

    public void accelerate(String string) {

        System.out.println("Car is accelerated!"+string);
       }


    public void speed() {
        System.out.println("Is an automatic. ");
    }
    public void speed(int speed) {
        System.out.println("Speed is "+ speed);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTypeOfBox() {
        return typeOfBox;
    }

    public void setTypeOfBox(String typeOfBox) {
        this.typeOfBox = typeOfBox;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return capacity == car.capacity &&
                color.equals(car.color) &&
                model.equals(car.model) &&
                typeOfBox.equals(car.typeOfBox);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, model, typeOfBox, capacity);
    }

    @Override
    public String toString() {
        StringBuilder str= new StringBuilder("Our ");
         str.append("Car is" +
                " " + color +
                ", model is " + model +
                ", typeOfBox is " + typeOfBox  +
                ", capacity is " + capacity );
        return str.toString();
    }


    @Override
    public void acceleration(String st) throws Exception {
        this.accelerate(st);
    }
}
