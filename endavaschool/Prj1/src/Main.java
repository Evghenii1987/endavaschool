public class Main {
    public static void main(String[] args) {
//Car and Man First
        Car car = new Car(String.valueOf(Color.Red), "Ford", "Manual",
                4, "Vasea", 3);
        Man man = new Man("Vasea", 30);

        System.out.println(man);
        System.out.println(car);
        man.accelerate("Yes");
        man.speed(5);

        System.out.println();

//Car and Man Second
        Car car1 = new Car(String.valueOf(Color.Red), "Ford", "Manual",
                4, "Vasea", 3);
        Man man1 = new Man("Vasea", 30);

        System.out.println(man1);
        System.out.println(car1);
        man1.accelerate("Yes");
        man1.speed(5);

        System.out.println(car == car1);
        System.out.println(car.equals(car1));
        System.out.println();

        //Car and Man Third
        Car car2 = new Car(String.valueOf(Color.Black), "Opel", "Automatic",
                4, "Ira", 6);
        Car man2 = new Man("Denis", 20);

        System.out.println(man2);
        System.out.println(car2);
        man2.accelerate("No");
        man2.speed();
        System.out.println();

        //Car and Man Fourth
        Car car3 = new Car(String.valueOf(Color.Silver), "Toyota", "Automatic",
                2, "Nicu", 1);
        Car man3 = new Man("Nicu", 25);

        System.out.println(man3);
        System.out.println(car3);
        man3.accelerate("Yes");
        man3.speed();
        System.out.println();

    }

    enum Color {
        Red, Green, Blue, Black, White, Silver;

    }
}
